using UnityEngine;

public class HealthCollectible : MonoBehaviour
{
    [SerializeField] private float healAmount;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            collision.GetComponent<Health>().GiveHeal(healAmount);
            gameObject.SetActive(false);
        }
    }
}
