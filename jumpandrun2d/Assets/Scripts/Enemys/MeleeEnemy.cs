using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeEnemy : MonoBehaviour
{
    [Header("Attack Parameters")]
    [SerializeField] private int attackDamage;
    [SerializeField] private float attackCooldown;

    [Header("Enemy Sight Parameters")]
    [SerializeField] private float enemySightStart;
    [SerializeField] private float enemySightRange;

    [Header("Player Layer")]
    [SerializeField] private BoxCollider2D boxCol;
    [SerializeField] private LayerMask playerLayer;

    [Header("Attack Sound")]
    [SerializeField] private AudioClip attackSound;

    private float cooldownTimer = Mathf.Infinity;
    private Animator anim;
    private Health playerHealth;

    private EnemyPatrol enemyPatrol;

    private void Awake()
    {
        enemyPatrol = GetComponentInParent<EnemyPatrol>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        cooldownTimer += Time.deltaTime;

        //Attack Player on sight
        if (PlayerInSight())
        {
            if (cooldownTimer > attackCooldown)
            {
                cooldownTimer = 0;
                anim.SetTrigger("meleeAttack");
                SoundManager.instance.PlaySound(attackSound);
            }
        }
        if (enemyPatrol != null)
        {
            enemyPatrol.enabled = !PlayerInSight();
        }
    }

    private void DamagePlayer()
    {
        //Only deal damage if Player still in range
        if(PlayerInSight())
        {
            playerHealth.TakeDamage(attackDamage);
        }
    }

    private bool PlayerInSight()
    {
        RaycastHit2D hit = Physics2D.BoxCast(boxCol.bounds.center + transform.right * enemySightStart * transform.localScale.x, 
            new Vector3(boxCol.bounds.size.x * enemySightRange, boxCol.bounds.size.y, boxCol.bounds.size.z), 0, Vector2.left, 0, playerLayer);
        if(hit.collider != null)
        {
            playerHealth = hit.collider.GetComponent<Health>();
        }
        return hit.collider != null;
    }

    /*
     * debug function to render the enemy sight
     */
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(boxCol.bounds.center + transform.right * enemySightStart * transform.localScale.x, new Vector3(boxCol.bounds.size.x * enemySightRange, boxCol.bounds.size.y, boxCol.bounds.size.z));
    }
}
