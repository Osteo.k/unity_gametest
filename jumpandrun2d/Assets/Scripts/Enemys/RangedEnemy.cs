using UnityEngine;

public class RangedEnemy : MonoBehaviour
{
    [Header("Attack Parameters")]
    [SerializeField] private int attackDamage;
    [SerializeField] private float attackCooldown;
    [SerializeField] private Transform firePoint;
    [SerializeField] private GameObject[] fireballs;

    [Header("Enemy Sight Parameters")]
    [SerializeField] private float enemySightStart;
    [SerializeField] private float enemySightRange;

    [Header("Player Layer")]
    [SerializeField] private BoxCollider2D boxCol;
    [SerializeField] private LayerMask playerLayer;

    [Header("Attack Sound")]
    [SerializeField] private AudioClip attackSound;

    private float cooldownTimer = Mathf.Infinity;
    private Animator anim;
    private EnemyPatrol enemyPatrol;

    private void Awake()
    {
        enemyPatrol = GetComponentInParent<EnemyPatrol>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        cooldownTimer += Time.deltaTime;

        //Attack Player on sight
        if (PlayerInSight())
        {
            if (cooldownTimer > attackCooldown)
            {
                cooldownTimer = 0;
                SoundManager.instance.PlaySound(attackSound);
                anim.SetTrigger("rangeAttack");
            }
        }
        if (enemyPatrol != null)
        {
            enemyPatrol.enabled = !PlayerInSight();
        }
    }

    private void RangedAttack()
    {
        cooldownTimer = 0;
        //pool arrows

        fireballs[FindFireball()].transform.position = firePoint.position;
        fireballs[FindFireball()].GetComponent<EnemyProjectile>().ActivateProjectile();
    }

    private int FindFireball()
    {
        for (int i = 0; i < fireballs.Length; i++)
        {
            if (!fireballs[i].activeInHierarchy)
                return i;
        }
        return 0;
    }

    private bool PlayerInSight()
    {
        RaycastHit2D hit = Physics2D.BoxCast(boxCol.bounds.center + transform.right * enemySightStart * transform.localScale.x,
            new Vector3(boxCol.bounds.size.x * enemySightRange, boxCol.bounds.size.y, boxCol.bounds.size.z), 0, Vector2.left, 0, playerLayer);

        return hit.collider != null;
    }

    /*
     * debug function renders enemy sight
     */
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(boxCol.bounds.center + transform.right * enemySightStart * transform.localScale.x, new Vector3(boxCol.bounds.size.x * enemySightRange, boxCol.bounds.size.y, boxCol.bounds.size.z));
    }
}
