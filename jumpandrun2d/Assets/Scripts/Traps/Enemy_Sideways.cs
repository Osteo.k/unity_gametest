using UnityEngine;

public class Enemy_Sideways: MonoBehaviour
{
    [SerializeField] private float damage;
    [SerializeField] private float speed;
    [SerializeField] private float moveRange;

    [Header("Attack Sound")]
    [SerializeField] private AudioClip trapSound;

    private bool moveleft;
    private float leftEdge;
    private float rightEdge;

    private void Awake()
    {
        leftEdge = transform.position.x - moveRange;
        rightEdge = transform.position.x + moveRange;
    }

    private void Update()
    {
        if (moveleft)
        {
            if(transform.position.x > leftEdge) 
            {
                transform.position = new Vector3(transform.position.x - speed * Time.deltaTime,transform.position.y,transform.position.z);
            }
            else
            {
                moveleft = false;
            }
        }
        else
        {
            if(transform.position.x < rightEdge)
            {
                transform.position = new Vector3(transform.position.x + speed * Time.deltaTime, transform.position.y, transform.position.z);
            }
            else
            {
                moveleft = true;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            SoundManager.instance.PlaySound(trapSound);
            collision.GetComponent<Health>().TakeDamage(damage);
        }
    }
}
