
using UnityEngine;

public class ArrowTrap : MonoBehaviour
{
    [SerializeField] private float attackCooldown;
    [SerializeField] private Transform arrowPoint;
    [SerializeField] private GameObject[] arrows;

    [Header("Attack Sound")]
    [SerializeField] private AudioClip arrowSound;
    private float coolDownTimer;

    private void Attack()
    {
        SoundManager.instance.PlaySound(arrowSound);
        coolDownTimer = 0;
        arrows[FindFireball()].transform.position = arrowPoint.position;
        arrows[FindFireball()].GetComponent<EnemyProjectile>().ActivateProjectile();
    }
    private int FindFireball()
    {
        for (int i = 0; i < arrows.Length; i++)
        {
            if (!arrows[i].activeInHierarchy)
            {
                return i;
            }
        }
        return 0;
    }

    private void Update()
    {
        coolDownTimer += Time.deltaTime;
        if(coolDownTimer >=attackCooldown)
        {
            Attack();
        }
    }
}
