using System.Collections;
using UnityEngine;

public class Portal : MonoBehaviour
{
    [SerializeField] private float teleportTime;
    [SerializeField] private Transform entryPortal;
    [SerializeField] private Transform conectedPortal;
    [SerializeField] private float scaleFactor;
    private bool recentlyUsed;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && !recentlyUsed)
        {
            // block PlayerMovement and add teleportation effects
            collision.GetComponent<PlayerMovement>().enabled = false;
            collision.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            collision.GetComponent<Rigidbody2D>().gravityScale = 0;
            collision.GetComponent<Animator>().SetBool("Run", false);
            collision.GetComponent<Health>().PortalInvunerability();

            // actual Teleportation
            StartCoroutine(ActivatePortal(collision));

            conectedPortal.GetComponent<Portal>().recentlyUsed = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        recentlyUsed = false;
    }

    private IEnumerator ActivatePortal(Collider2D collision)
    {
        Vector3 oldSize = collision.transform.localScale;
        Vector3 oldPos = collision.transform.position;
        Vector3 newSize = new Vector3(scaleFactor * collision.transform.localScale.x, scaleFactor, scaleFactor);

        float currentTime = 0;
        float gravity = collision.GetComponent<Rigidbody2D>().gravityScale;

        // shrinks and moves the player towards the middle of the portal
        while (currentTime < teleportTime)
        {
            currentTime += Time.deltaTime;
            collision.transform.localScale = Vector3.Lerp(oldSize, newSize, currentTime/teleportTime);
            collision.transform.position = Vector3.Lerp(oldPos, entryPortal.position, currentTime / teleportTime);
            yield return null;
        }

        // change position to corresponding Portal and reverse portal effects
        collision.transform.position = conectedPortal.position;
        collision.GetComponent<Health>().PortalInvunerability();
        currentTime = 0;

        // regrows Player to original size
        while (currentTime < teleportTime)
        {
            currentTime += Time.deltaTime;
            collision.transform.localScale = Vector3.Lerp(newSize, oldSize, currentTime / teleportTime);
            yield return null;
        }

        // reenable playermovement
        collision.GetComponent<PlayerMovement>().enabled = true;
        collision.GetComponent<Rigidbody2D>().gravityScale = gravity;
    }
}
