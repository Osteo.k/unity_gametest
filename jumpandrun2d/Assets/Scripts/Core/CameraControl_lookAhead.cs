using UnityEngine;

public class CameraControl_lookAhead : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private float aheadDistance;
    [SerializeField] private float cameraSpeed;
    private float lookAhead;

    private void Update()
    {
        transform.position = new Vector3(player.transform.position.x + lookAhead,transform.position.y, transform.position.z);

        // adds transition effect, so that cameras moves towards viewing direction, so player can see more of whats in front of him
        lookAhead = Mathf.Lerp(lookAhead, (aheadDistance * player.localScale.x), Time.deltaTime * cameraSpeed);  
    }
}
