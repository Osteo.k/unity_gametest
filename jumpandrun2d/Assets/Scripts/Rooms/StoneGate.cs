
using UnityEngine;

public class StoneGate : MonoBehaviour
{
    private bool opened;
    [SerializeField] private Transform movePoint;
    [SerializeField] private float shakePower;
    [SerializeField] private float speed;
    private Transform originalPos;
    private void Awake()
    {
        opened = false;
        originalPos = transform;
    }
    void Update()
    {
        if (opened)
        {
            MoveGate();
        }
    }
    public void LeverActivated()
    {
        opened = true;
    }
    private void MoveGate()
    {
        if(Vector3.Distance(transform.position, movePoint.position)<0.5f){
            opened = false;
        }
        else
        {
            // add a shake effect to the stoneGate opening
            Vector2 shakePos = Random.insideUnitCircle * shakePower;
            float movementSpeed = speed * Time.deltaTime + shakePos.y;
            transform.position = new Vector3(originalPos.position.x + (shakePos.x/2), transform.position.y - movementSpeed, transform.position.z);
        }
    }
    private void OnDisable()
    {
        opened = false;
    }
}
