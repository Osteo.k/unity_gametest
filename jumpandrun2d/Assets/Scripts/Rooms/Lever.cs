using UnityEngine;

public class Lever : MonoBehaviour
{
    private BoxCollider2D boxCol;
    [SerializeField] private GameObject stoneGate;
    [SerializeField] private LayerMask PlayerLayer;
    //private bool onLever;

    private void Awake()
    {
        boxCol = GetComponent<BoxCollider2D>();
        //onLever = false;
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.E) && onLever())
        {
            stoneGate.GetComponent<StoneGate>().LeverActivated();
        }
    }

    // first try, does also work but the other solution is simplier
    /*private void OnTriggerEnter2D(Collider2D collision)
    {
        onLever = true;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        onLever = false;
    }*/

    /*
     * hit detection to see if player is on the lever hitbox
     */
    private bool onLever()
    {
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxCol.bounds.center, boxCol.bounds.size, 0, new Vector2(transform.localScale.x, 0), 0.1f, PlayerLayer);
        return raycastHit.collider != null;
    }

    private void OnDisable()
    {
        //onLever = false;
    }
}
